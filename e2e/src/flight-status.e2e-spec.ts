import { FlightStatusPage } from './flight-status.po'
import { browser, logging, by, element, ElementFinder } from 'protractor'

describe('workspace-project App', () => {
  let page: FlightStatusPage

  beforeEach(() => {
    page = new FlightStatusPage()
  })

  it('should go to flight statuses page', () => {
    page.navigateTo()
    expect(element(by.css('.flight-status')).isPresent()).toBe(true)
  })

  it('should be 5 items initialy', () => {
    page.navigateTo()
    const tableElem = element(by.css('table'))
    page.waitVisibilityOf(tableElem)
    element.all(by.css('table .mat-row')).then(totalRows => {
      expect(totalRows.length).toBe(5)
    })
  })

  it('should be 10 items after selecting 10 from drowdown', () => {
    page.navigateTo()
    const selectPanel = element(by.css('.mat-select'))
    page.waitVisibilityOf(selectPanel)
    selectPanel.click().then(() => {
      const option = element(by.cssContainingText('.mat-option-text', '10'))
      page.waitVisibilityOf(option)
      option.click().then(() => {
        element.all(by.css('table .mat-row')).then(totalRows => {
          expect(totalRows.length).toBe(10)
        })
      })
    })
  })

  it('should be next 10 items after clicking next page', () => {
    page.navigateTo()
    let firstElement: ElementFinder
    const tableElem = element(by.css('table'))
    page.waitVisibilityOf(tableElem, 5000)
    element.all(by.css('table .mat-row')).then(totalRows => {
      firstElement = totalRows[0]
    })

    const nextBtn = element(by.css('.mat-paginator-navigation-next'))
    page.waitVisibilityOf(nextBtn)
    nextBtn.click().then(() => {
      element.all(by.css('table .mat-row')).then(totalRows => {
        expect(totalRows[0]).not.toEqual(firstElement)
      })
    })
  })

  afterEach(async () => {
    const logs = await browser
      .manage()
      .logs()
      .get(logging.Type.BROWSER)
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    )
  })
})
