import { browser, ExpectedConditions as until, ElementFinder } from 'protractor';

export class FlightStatusPage {
  navigateTo() {
    return browser.get('/flight-status') as Promise<any>;
  }

  waitVisibilityOf(elem: ElementFinder, time: number = 5000) {
    browser.wait(until.visibilityOf(elem), time);
  }
}
