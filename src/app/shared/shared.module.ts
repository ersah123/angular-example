import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component'
import { MatProgressSpinnerModule, MatCardModule } from '@angular/material'

@NgModule({
  declarations: [LoadingSpinnerComponent],
  imports: [CommonModule, MatProgressSpinnerModule, MatCardModule],
  exports: [LoadingSpinnerComponent],
})
export class SharedModule {}
