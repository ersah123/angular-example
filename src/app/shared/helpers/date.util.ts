// formats default date toISOString to YYYY-MM-DDTHH:MM:SSZ
export const formatDateYYYYMMDDHHMMSS = (dateISO: string): string => {
  return (
    dateISO.slice(0, 19) + dateISO[dateISO.length - 1] // 19 is for stripping of the milliseconds from date string
  )
}

// returns date from today by passed amount of days
export const getDateFromToday = (dayAmount: number = 0): string => {
  const date = new Date()
  const dateISO: string = new Date(
    date.setDate(date.getDate() + dayAmount)
  ).toISOString()
  return formatDateYYYYMMDDHHMMSS(dateISO)
}
