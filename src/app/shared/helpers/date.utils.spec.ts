import { getDateFromToday } from 'src/app/shared/helpers/date.util'
import { formatDateYYYYMMDDHHMMSS } from './date.util'

describe('Date utils', () => {
  it('should return formatted date', () => {
    const now = new Date()
    const ISONow = formatDateYYYYMMDDHHMMSS(now.toISOString())
    const dateNow = getDateFromToday()

    expect(dateNow).toEqual(ISONow)
  })
})
