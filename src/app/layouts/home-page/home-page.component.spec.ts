import { RouterTestingModule } from '@angular/router/testing'
import { MatMenuModule, MatToolbarModule } from '@angular/material'
import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { HomePageComponent } from './home-page.component'
import { NavComponent } from '../nav/nav.component'

describe('HomePageComponent', () => {
  let component: HomePageComponent
  let fixture: ComponentFixture<HomePageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomePageComponent, NavComponent],
      imports: [RouterTestingModule, MatMenuModule, MatToolbarModule],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
