import { HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core'
import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
} from '@angular/material'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { CoreModule } from './core/core.module'
import { HomePageComponent } from './layouts/home-page/home-page.component'
import { NavComponent } from './layouts/nav/nav.component'

@NgModule({
  declarations: [AppComponent, HomePageComponent, NavComponent],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    CoreModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
