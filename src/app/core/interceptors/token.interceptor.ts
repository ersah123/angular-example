import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http'

import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { environment as env } from '@env/environment'

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Accept: 'application/hal+json;version=com.afkl.operationalflight.v3',
        'api-key': env.apiKey,
      },
    })
    return next.handle(request)
  }
}
