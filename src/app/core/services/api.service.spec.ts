import { TokenInterceptor } from './../interceptors/token.interceptor'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { TestBed } from '@angular/core/testing'

import { ApiService } from './api.service'

describe('ApiService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true,
        },
      ],
    })
  )

  it('should be created', () => {
    const service: ApiService = TestBed.get(ApiService)
    expect(service).toBeTruthy()
  })
})
