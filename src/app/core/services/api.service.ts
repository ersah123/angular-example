import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs'

import { Injectable } from '@angular/core'
import { environment as env } from '@env/environment'

const BASE_URL = env.domainUrl + env.apiPrefix

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private httpClient: HttpClient) {}

  public get<T>(
    path: string,
    params: HttpParams = new HttpParams()
  ): Observable<T> {
    return this.httpClient.get<T>(BASE_URL + path, { params })
  }

  public put<T>(path: string, body: object = {}): Observable<T> {
    return this.httpClient.put<T>(BASE_URL + path, JSON.stringify(body))
  }

  public post<T>(path: string, body: object = {}): Observable<T> {
    return this.httpClient.post<T>(BASE_URL + path, JSON.stringify(body))
  }

  public delete<T>(path: string): Observable<T> {
    return this.httpClient.delete<T>(BASE_URL + path)
  }
}
