import { HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './api.service'

@Injectable({
  providedIn: 'root',
})
export class FlightStatusService {
  constructor(private apiService: ApiService) {}

  getAll<T>(
    startDate: string,
    tomorrow: string,
    itemCount: number,
    currentPageIndex: number,
    searchValue: string
  ): Observable<T> {
    const params = new HttpParams()
      .set('startRange', startDate)
      .set('endRange', tomorrow)
      .set('pageSize', itemCount.toString())
      .set('pageNumber', currentPageIndex.toString())
      .set('flightNumber', searchValue)

    return this.apiService.get<T>('/flightstatus', params)
  }
}
