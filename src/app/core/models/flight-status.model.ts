export interface FlightStatus {
  operationalFlights: Flight[]
  page: {
    fullCount: number
    pageCount: number
    pageNumber: number
    pageSize: number
    totalPages: number
  }
}

export interface Flight {
  flightNumber: string
  flightScheduleDate: number
  flightStatusPublicLangTransl: number
  airline: {
    name: string
  }
}
