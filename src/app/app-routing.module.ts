import { RouterModule, Routes } from '@angular/router'

import { HomePageComponent } from './layouts/home-page/home-page.component'
import { NgModule } from '@angular/core'

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: '',
    component: HomePageComponent,
    children: [
      {
        path: 'home',
        loadChildren: () =>
          import('./modules/home/home.module').then(m => m.HomeModule),
      },
      {
        path: 'flight-status',
        loadChildren: () =>
          import('./modules/flight-status/flight-status.module').then(
            m => m.FlightStatusModule
          ),
      },
    ],
  },
  // Fallback when no prior routes is matched
  { path: '**', redirectTo: '/home', pathMatch: 'full' },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
