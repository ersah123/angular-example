import { RouterModule, Routes } from '@angular/router'

import { FlightStatusComponent } from './pages/flight-status.component'
import { NgModule } from '@angular/core'

export const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: FlightStatusComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FlightStatusRoutingModule {}
