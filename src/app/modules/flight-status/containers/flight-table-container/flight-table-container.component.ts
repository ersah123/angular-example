import { Component, OnDestroy, OnInit } from '@angular/core'
import { MatTableDataSource, PageEvent } from '@angular/material'
import { Subscription } from 'rxjs'
import { FlightStatus } from 'src/app/core/models'
import { Flight } from 'src/app/core/models/flight-status.model'
import { FlightStatusService } from 'src/app/core/services/flight-status.service'
import { getDateFromToday } from 'src/app/shared/helpers/date.util'

@Component({
  selector: 'app-flight-table-container',
  templateUrl: './flight-table-container.component.html',
  styleUrls: ['./flight-table-container.component.scss'],
})
export class FlightTableContainerComponent implements OnInit, OnDestroy {
  currentPageSize = 5
  currentPageIndex = 0
  currentLength = 0

  flightStatuses$!: Subscription
  flightStatuses = new MatTableDataSource<Flight>()

  isLoading = true
  startDate = getDateFromToday()
  endDate = getDateFromToday(1)
  searchValue = ''

  constructor(private flightStatusService: FlightStatusService) {}

  ngOnInit() {
    this.loadFlightStatuses()
  }

  onItemCountChange($event: PageEvent) {
    this.currentPageSize = $event.pageSize
    this.currentPageIndex = $event.pageIndex
    this.loadFlightStatuses()
  }

  onSearch(value: string) {
    console.log('value', value)
    this.searchValue = value
    // this.loadFlightStatuses()
  }

  loadFlightStatuses() {
    this.isLoading = true

    this.flightStatuses$ = this.flightStatusService
      .getAll<FlightStatus>(
        this.startDate,
        this.endDate,
        this.currentPageSize,
        this.currentPageIndex,
        this.searchValue
      )
      .subscribe((status: FlightStatus) => {
        this.flightStatuses.data = status.operationalFlights
        this.currentLength = status.page.totalPages
        this.isLoading = false
      })
  }

  ngOnDestroy() {
    this.flightStatuses$.unsubscribe()
  }
}
