import { HttpClientModule } from '@angular/common/http'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import {
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
} from '@angular/material'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { RouterTestingModule } from '@angular/router/testing'
import { of } from 'rxjs'
import { SharedModule } from 'src/app/shared/shared.module'
import { FlightSearchComponent } from '../../components/flight-search/flight-search.component'
import { FlightStatusService } from './../../../../core/services/flight-status.service'
import { FlightTablePaginationComponent } from './../../components/flight-table-pagination/flight-table-pagination.component'
import { FlightTableComponent } from './../../components/flight-table/flight-table.component'
import { FlightTableContainerComponent } from './flight-table-container.component'

const payload = {
  operationalFlights: [],
  page: {
    totalPages: 10,
    fullCount: 1000,
    pageCount: 10,
    pageNumber: 1,
    pageSize: 10,
  },
}

describe('FlightTableContainerComponent', () => {
  let component: FlightTableContainerComponent
  let fixture: ComponentFixture<FlightTableContainerComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FlightTableContainerComponent,
        FlightSearchComponent,
        FlightTableComponent,
        FlightTablePaginationComponent,
      ],
      imports: [
        SharedModule,
        RouterTestingModule,
        MatFormFieldModule,
        MatTableModule,
        MatPaginatorModule,
        HttpClientModule,
        MatInputModule,
        BrowserAnimationsModule,
      ],
      providers: [FlightStatusService],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents()
  }))

  beforeEach(() => {
    const flightStatusService = TestBed.get(FlightStatusService)
    spyOn(flightStatusService, 'getAll').and.returnValue(of(payload))
    fixture = TestBed.createComponent(FlightTableContainerComponent)
    component = fixture.componentInstance
  })

  it('should set data to component fields', () => {
    expect(component.isLoading).toBe(true)
    expect(component.currentLength).toEqual(0)
    fixture.detectChanges()
    expect(component.isLoading).toBe(false)
    expect(component.currentLength).toEqual(payload.page.totalPages)
    expect(component.flightStatuses.data).toEqual(payload.operationalFlights)
  })

  it('should create', () => {
    fixture.detectChanges()
    expect(component).toBeTruthy()
  })
})
