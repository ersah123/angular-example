import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { MatPaginatorModule, PageEvent } from '@angular/material'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FlightTablePaginationComponent } from './flight-table-pagination.component'

const pageEvent: PageEvent = {
  length: 944,
  pageIndex: 0,
  pageSize: 10,
  previousPageIndex: 0,
}

describe('FlightTablePaginationComponent', () => {
  let component: FlightTablePaginationComponent
  let fixture: ComponentFixture<FlightTablePaginationComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FlightTablePaginationComponent],
      imports: [MatPaginatorModule, BrowserAnimationsModule],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightTablePaginationComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should render pagination container', () => {
    const paginationContainer = fixture.nativeElement.querySelector(
      '.mat-paginator-container'
    )
    expect(paginationContainer).toBeTruthy()
  })

  it('should emit page event', () => {
    spyOn(component.itemCountChange, 'emit')
    component.onItemCountChange(pageEvent)
    expect(component.itemCountChange.emit).toHaveBeenCalledWith(pageEvent)
  })
})
