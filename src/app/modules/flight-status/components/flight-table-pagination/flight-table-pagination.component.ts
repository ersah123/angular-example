import { Component, EventEmitter, Input, Output } from '@angular/core'
import { PageEvent } from '@angular/material'

@Component({
  selector: 'app-flight-table-pagination',
  templateUrl: './flight-table-pagination.component.html',
  styleUrls: ['./flight-table-pagination.component.scss'],
})
export class FlightTablePaginationComponent {
  pageSizeOptions = [5, 10, 20]

  @Input()
  currentPageIndex = 0

  @Input()
  currentLength = 0

  @Input()
  currentPageSize = 5

  @Output()
  itemCountChange = new EventEmitter<PageEvent>()

  onItemCountChange($event: PageEvent) {
    this.itemCountChange.emit($event)
  }
}
