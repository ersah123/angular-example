import { MatTableDataSource } from '@angular/material'
import { Flight } from 'src/app/core/models'
import { Component, OnInit, Input } from '@angular/core'

@Component({
  selector: 'app-flight-table',
  templateUrl: './flight-table.component.html',
  styleUrls: ['./flight-table.component.scss'],
})
export class FlightTableComponent implements OnInit {
  displayedColumns: string[] = [
    'flightNumber',
    'flightScheduleDate',
    'flightStatusPublicLangTransl',
    'airline.name',
  ]

  @Input()
  flights!: MatTableDataSource<Flight>

  constructor() {}

  ngOnInit() {}
}
