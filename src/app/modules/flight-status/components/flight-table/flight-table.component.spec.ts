import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { MatPaginatorModule, MatTableModule } from '@angular/material'
import { FlightTableComponent } from './flight-table.component'

describe('FlightTableComponent', () => {
  let component: FlightTableComponent
  let fixture: ComponentFixture<FlightTableComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FlightTableComponent],
      imports: [MatTableModule, MatPaginatorModule],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightTableComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should render the table', () => {
    let tableRows = fixture.nativeElement.querySelectorAll('tr')

    // Header row
    let headerRow = tableRows[0]
    expect(headerRow.cells[0].innerHTML).toBe('Flight Number')
    expect(headerRow.cells[1].innerHTML).toBe('Flight Schedule Date')
    expect(headerRow.cells[2].innerHTML).toBe('Flight Status')
    expect(headerRow.cells[3].innerHTML).toBe('Airline')
  })
})
