import { Component, EventEmitter, Output } from '@angular/core'
import { Subject } from 'rxjs'
import { debounceTime } from 'rxjs/operators'

@Component({
  selector: 'app-flight-search',
  templateUrl: './flight-search.component.html',
  styleUrls: ['./flight-search.component.scss'],
})
export class FlightSearchComponent {
  @Output()
  searchChange = new EventEmitter<string>()
  inputs: Subject<string> = new Subject()
  debounceTime = 300

  ngOnInit() {
    this.inputs
      .pipe(debounceTime(this.debounceTime))
      .subscribe(e => this.searchChange.emit(e))
  }

  onSearch(value: string) {
    this.inputs.next(value)
  }
}
