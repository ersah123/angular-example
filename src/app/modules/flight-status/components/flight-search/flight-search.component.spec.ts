import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing'
import { MatFormFieldModule, MatInputModule } from '@angular/material'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FlightSearchComponent } from './flight-search.component'

describe('FlightSearchComponent', () => {
  let component: FlightSearchComponent
  let fixture: ComponentFixture<FlightSearchComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FlightSearchComponent],
      imports: [MatFormFieldModule, MatInputModule, BrowserAnimationsModule],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightSearchComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should render input tag', () => {
    const inputTag = fixture.nativeElement.querySelectorAll('input')
    expect(inputTag).toBeTruthy()
  })

  it('should emit search values', fakeAsync(() => {
    spyOn(component.searchChange, 'emit')
    const searchValue = '12'
    component.onSearch(searchValue)
    tick(component.debounceTime)
    expect(component.searchChange.emit).toHaveBeenCalledWith(searchValue)
  }))
})
