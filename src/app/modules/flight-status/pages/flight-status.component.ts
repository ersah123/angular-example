import { Component } from '@angular/core'

@Component({
  selector: 'app-flight',
  templateUrl: './flight-status.component.html',
  styleUrls: ['./flight-status.component.scss'],
})
export class FlightStatusComponent {}
