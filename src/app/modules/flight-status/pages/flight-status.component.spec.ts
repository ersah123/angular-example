import { HttpClientModule } from '@angular/common/http'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing'
import { Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { FlightTableContainerComponent } from './../containers/flight-table-container/flight-table-container.component'
import { FlightStatusComponent } from './flight-status.component'

describe('FlightStatusComponent', () => {
  let component: FlightStatusComponent
  let fixture: ComponentFixture<FlightStatusComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FlightStatusComponent, FlightTableContainerComponent],
      imports: [RouterTestingModule, HttpClientModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightStatusComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should navigate to a flight statuses', fakeAsync(() => {
    const router = TestBed.get(Router)

    router.navigateByUrl('')
    advance(fixture)

    const compiled = fixture.debugElement.nativeElement
    expect(compiled.querySelector('h2').textContent).toContain(
      'Flight Status table'
    )
  }))

  function advance(f: ComponentFixture<any>) {
    tick()
    f.detectChanges()
  }
})
