import { FlightTableComponent } from './components/flight-table/flight-table.component'
import {
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
} from '@angular/material'

import { CommonModule } from '@angular/common'
import { FlightStatusComponent } from './pages/flight-status.component'
import { FlightStatusRoutingModule } from './flight-status.routing'
import { HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { FlightTableContainerComponent } from './containers/flight-table-container/flight-table-container.component'
import { FlightTablePaginationComponent } from './components/flight-table-pagination/flight-table-pagination.component'
import { FlightSearchComponent } from './components/flight-search/flight-search.component'
import { SharedModule } from 'src/app/shared/shared.module'

@NgModule({
  declarations: [
    FlightStatusComponent,
    FlightTableComponent,
    FlightTableContainerComponent,
    FlightTablePaginationComponent,
    FlightSearchComponent,
  ],
  imports: [
    CommonModule,
    FlightStatusRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    HttpClientModule,
    MatInputModule,
    MatFormFieldModule,
    SharedModule,
  ],
})
export class FlightStatusModule {}
