import { CommonModule } from '@angular/common'
import { HomeComponent } from './pages/home.component'
import { HomeRoutingModule } from './home.routing'
import { NgModule } from '@angular/core'

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, HomeRoutingModule],
})
export class HomeModule {}
