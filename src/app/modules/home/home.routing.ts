import { RouterModule, Routes } from '@angular/router'

import { HomeComponent } from './pages/home.component'
import { NgModule } from '@angular/core'

export const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: HomeComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
