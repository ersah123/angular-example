# Onboarding

- Make sure your 4200 port is not already busy on your localhost
- Run `ng serve` command insoie your project root
- go to http://localhost:4200
- go to http://localhost:9876/ for unit tests
- execute `ng e2e` for end to end tests

## With Docker

- Make sure your 4200 port is not already busy on your localhost
- Run `docker-compose up` command on project root directory (for to start single container run something like this `docker-compose up klm`)
- go to http://localhost:4200
- go to http://localhost:9876/ for unit tests
- end to end tests will already run headlessly in container

To run npm commands, will be best to go to containers shell and execute commands from there.

Tip: You can use VSCode's docker extension to attach shell
